package main

import (
	"log"
	"net/http"
	"time"
)

func getCurrentTime() string {
	return time.Now().String()
}

func hello() string {
	return "hello"
}

func main() {
	http.HandleFunc("/time", func(writer http.ResponseWriter, request *http.Request) {
		if _, err := writer.Write([]byte(getCurrentTime())); err != nil {
			log.Print("error calling /time: " + err.Error())
		}
	})

	http.HandleFunc("/hello", func(writer http.ResponseWriter, request *http.Request) {
		if _, err := writer.Write([]byte(hello())); err != nil {
			log.Print("error calling /hello: " + err.Error())
		}
	})

	if err := http.ListenAndServe(":8000", nil); err != nil {
		log.Print("error", err)
	}
}
