run_linter:
	golangci-lint run -c .golangci.yml

build_program:
	go build -o build/program main.go

build_program_macos:
	GOOS=darwin go build -o build/program.go main.go

build_program_linux:
	GOOS=linux CGO_ENABLED=0 go build -ldflags "-s -w" -o build/program main.go

test:
	go test -v -coverprofile=coverage.out .

test_coverage:
	go tool cover -func=coverage.out